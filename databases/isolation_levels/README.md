# Isolation levels


## References
[Mark McDonald - Fixing a race condition](https://medium.com/in-the-weeds/fixing-a-race-condition-c8b475fbb994)
[PostgreSQL - Transaction isolation](https://www.postgresql.org/docs/9.6/transaction-iso.html)
[PostgreSQL - Advisory locks](https://www.postgresql.org/docs/9.6/explicit-locking.html#ADVISORY-LOCKS)
