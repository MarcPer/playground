# frozen_string_literal: true

BEGIN {
  require 'sequel'
  DB = Sequel.postgres(host: 'postgres', user: 'postgres')
  # DB = Sequel.mysql2(host: 'mysql', user: 'root', password: 'root', database: 'mysql')
  DB.create_table! :accounts do # with the !, drop table first if it exists
    primary_key :id
    String :country
    BigDecimal :balance, size: [10, 2]
  end
}

END {
  DB.drop_table? :accounts
}

# # SELECT FOR UPDATE
# DB.transaction(isolation: :serializable) do
#   acc = DB[:accounts].for_update.first(id: 1)

#   acc.update(:name=>'RF')
# end

require_relative './colorize'

class App
  extend Colorize

  def self.output(text)
    color = Thread.current[:color]
    prefix = Thread.current[:prefix]
    colorize(prefix, color, text)
  end

  def self.run(demo_name, isolation)
    puts "==== Starting #{demo_name} demonstration with #{isolation} isolation"
    truncate
    send(demo_name, isolation)
    puts "===="
    puts ''
  end

  def self.truncate
    DB.create_table! :accounts do # with the !, drop table first if it exists
      primary_key :id
      String :country
      BigDecimal :balance, size: [10, 2]
    end
  end

  # Isolation levels
  # :serializable, :repeatable, :committed, :uncommitted
  def self.dirty_read(isolation)
    mutex = Mutex.new
    resource = ConditionVariable.new
    resource2 = ConditionVariable.new
    DB[:accounts].insert(balance: 10.00)

    threads = []
    threads << Thread.new do
      Thread.current[:color] = Colorize::CYAN
      Thread.current[:prefix] = 'thread_a'
      DB.transaction(isolation: isolation) do
        mutex.synchronize do
          output "First read #{DB[:accounts].first[:balance]}"
          resource.wait(mutex)
          output "Second read: #{DB[:accounts].first[:balance]}"
          resource2.signal
        end
      end
    end

    sleep 0.5
    threads << Thread.new do
      Thread.current[:color] = Colorize::GREEN
      Thread.current[:prefix] = 'thread_b'
      mutex.synchronize do
        output 'Update'
        DB.transaction(isolation: isolation) do
          DB[:accounts].where(id: 1).update(balance: Sequel[:balance] - 7)
          output DB[:accounts].first[:balance]
          resource.broadcast
          resource2.wait(mutex)
          output 'End'
        end
      end
    end

    threads.each(&:join)
    puts "Final balance: #{DB[:accounts].first[:balance]}"
  end

  # Isolation levels
  # :serializable, :repeatable, :committed, :uncommitted
  def self.phantom_read(isolation)
    mutex = Mutex.new
    resource = ConditionVariable.new
    DB[:accounts].insert(country: 'DE', balance: 53.20)
    DB[:accounts].insert(country: 'DE', balance: 10.60)
    DB[:accounts].insert(country: 'US', balance: 25.00)

    threads = []
    threads << Thread.new do
      Thread.current[:color] = Colorize::CYAN
      Thread.current[:prefix] = 'thread_a'
      DB.transaction(isolation: isolation) do
        mutex.synchronize do
          output 'First read'
          DB[:accounts].where_each(country: 'DE') { |row| output row }
          resource.wait(mutex)
          output 'Second read'
          DB[:accounts].where_each(country: 'DE') { |row| output row }
        end
      end
    end

    sleep 0.5
    threads << Thread.new do
      Thread.current[:color] = Colorize::GREEN
      Thread.current[:prefix] = 'thread_b'
      mutex.synchronize do
        output 'start'
        DB.transaction(isolation: isolation) do
          DB[:accounts].insert(country: 'DE', balance: 99.91)
        end
        resource.signal
        output 'end'
      end
    end

    threads.each(&:join)
  end

  def self.serialization_anomaly(isolation)
    mutex = Mutex.new
    resource = ConditionVariable.new
    DB[:accounts].insert(country: 'DE', balance: 53.20)
    DB[:accounts].insert(country: 'DE', balance: 10.60)
    DB[:accounts].insert(country: 'US', balance: 25.00)

    threads = []
    threads << Thread.new do
      Thread.current[:color] = Colorize::CYAN
      Thread.current[:prefix] = 'thread_a'
      DB.transaction(isolation: isolation) do
        mutex.synchronize do
          balance = DB[:accounts].where(country: 'DE').sum(:balance)
          output "Total DE balance: #{balance}"
          DB[:accounts].insert(country: 'US', balance: balance)
          resource.wait(mutex)
        end
      end
    end

    sleep 0.5
    threads << Thread.new do
      Thread.current[:color] = Colorize::GREEN
      Thread.current[:prefix] = 'thread_b'
      DB.transaction(isolation: isolation) do
        mutex.synchronize do
          balance = DB[:accounts].where(country: 'US').sum(:balance)
          output "Total DE balance: #{balance}"
          DB[:accounts].insert(country: 'DE', balance: balance)
          resource.signal
        end
      end
    end

    threads.each(&:join)
    us_balance = DB[:accounts].where(country: 'US').sum(:balance)
    de_balance = DB[:accounts].where(country: 'DE').sum(:balance)
    puts "Final balances. US: #{us_balance}\tDE: #{de_balance}"
  end
end

# Isolation levels
# :serializable, :repeatable, :committed, :uncommitted
App.run(:dirty_read, :uncommitted)
App.truncate
App.run(:phantom_read, :repeatable)
App.truncate
App.run(:serialization_anomaly, :repeatable)

# What happens if
# One thread acquires the lock
# The other thread updates something
# The first thread releases the lock without updating anything
# The second thread can apply the conflicting changes.

# SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
# BEGIN;
# UPDATE accounts SET balance = balance + 5 WHERE id = 1;
# COMMIT;

# INSERT INTO accounts (country, balance) VALUES ('DE', 10);
