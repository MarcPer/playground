# frozen_string_literal: true

module Colorize
  CYAN = "\e[36m"
  GREEN = "\e[32m"
  RESET = "\e[0m"

  def colorize(prefix, color, text)
    puts color
    print "#{prefix}: "
    puts text
    puts RESET
  end

  def green(prefix, text)
    colorize(prefix, GREEN, text)
  end

  def cyan(prefix, text)
    colorize(prefix, CYAN, text)
  end
end
