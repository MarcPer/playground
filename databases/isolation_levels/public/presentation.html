<!DOCTYPE html>
<html>
  <head>
    <title>SQL transaction isolation levels</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }

      tbody tr:nth-child(odd) {
        background-color: #dbf0ff;
      }

      code.SQL { font-size: 0.7em; }

      .footnote {
        position: absolute;
        bottom: 3em;
      }

      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }

      .blockquote {
        font-style: italic;
        border-left: 3px solid #ccc;
        padding-left: 10px;
        padding-right: 10px;
      }

      .pull-left {
        float: left;
        width: 47%;
      }

      .pull-right {
        float: right;
        width: 47%;
      }

      .red {
        color: #b93f3f;
      }
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle

# SQL Transaction isolation levels

---

# Agenda

1. Why?
2. Isolation levels standard
3. Anomalies
  1. Dirty read
  2. Nonrepeatable read
  3. Phantom read
  4. Serialization anomaly
4. Explicit locking

---

# Why?

Because concurrency. List of anomalies from PostgreSQL docs:

1. **Dirty read:** A transaction reads data written by a concurrent uncommitted transaction.

2. **Nonrepeatable read:** A transaction re-reads data it has previously read and finds that data has been modified by another transaction (that committed since the initial read).

3. **Phantom read:** A transaction re-executes a query returning a set of rows that satisfy a search condition and finds that the set of rows satisfying the condition has changed due to another recently-committed transaction.

4. **Serialization anomaly:** The result of successfully committing a group of transactions is inconsistent with all possible orderings of running those transactions one at a time.

---

# Isolation levels (the ANSI/ISO standard)

Isolation Level  | Dirty Read   | Nonrepeatable Read | Phantom Read | Serialization Anomaly |
---------------- | ------------ | ------------------ | ------------ | --------------------- |
Read uncommitted | Allowed*     | Possible           | Possible     | Possible              |
Read committed   | Not possible | Possible           | Possible     | Possible              |
Repeatable read  | Not possible | Not possible       | Allowed**    | Possible              |
Serializable     | Not possible | Not possible       | Not possible | Not possible          |

.footnote[
  <p>* Not possible in PostgreSQL</p>
  <p>** Not possible in PostgreSQL or MySQL</p>
]

---

# Dirty read

<blockquote class="blockquote">A transaction reads data written by a concurrent uncommitted transaction.</blockquote>

This can happen in _Read uncommitted_ level:
.pull-left[
.red[Thread A]
```SQL
BEGIN;
SELECT balance FROM accounts
  WHERE id = 1; -- 100
--
--
SELECT balance FROM accounts
  WHERE id = 1; -- 150
COMMIT;
```

]
.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
--
UPDATE accounts SET balance = balance + 50
  WHERE id = 1;
--
--
COMMIT;
```
]

What's the problem?
1. You trust the other transaction will commit and run a business logic.
2. The other transaction rolls back, and your initial assumption ends up not being valid.

---
# Solving Dirty read

If isolation level is higher than _Read uncommitted_:
.pull-left[
.red[Thread A]
```SQL
BEGIN;
SELECT balance FROM accounts
  WHERE id = 1; -- 100
--
--
SELECT balance FROM accounts
  WHERE id = 1; -- 100
COMMIT;
```

]
.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
--
UPDATE accounts SET balance = balance + 50
  WHERE id = 1;
--
--
COMMIT;
```
]

When reading, _Thread A_ just sees committed transactions.

With _Read committed_ level, the database is frozen to what it was at the beginning of each _SQL statement_.

---

# A note on concurrent updates

Updates always lock the rows until transaction is committed.

.pull-left[
.red[Thread A]
```SQL
BEGIN;
UPDATE accounts SET balance = balance + 50
  WHERE id = 1; -- UPDATE OK
--
--
--
--
COMMIT;
```

]
.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
--
UPDATE accounts SET balance = balance + 50
  WHERE id = 1;
-- HANG
-- HANG
-- UPDATE OK
COMMIT;
```
]

<div style="clear: both;">The lock also applies to <code class="remark-inline-code">DELETE</code>.</div>

---

# Nonrepeatable read

<blockquote class="blockquote">
A transaction re-reads data it has previously read and finds that data has been modified by another transaction (that committed since the initial read).
</blockquote>

.pull-left[
.red[Thread A]
```SQL
BEGIN;
SELECT balance FROM accounts
  WHERE id = 1; -- 100
--
--
SELECT balance FROM accounts
  WHERE id = 1; -- 100
--
SELECT balance FROM accounts
  WHERE id = 1; -- 150
COMMIT;
```

]
.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
--
UPDATE accounts SET balance = balance + 50
  WHERE id = 1;
--
--
COMMIT;
--
--
--
```
]

<div style="clear: both;">So far so good. What's the problem? Let's see.</div>

---

# Nonrepeatable read problems

This can happen in the _Read committed_ level:

Assume we have two rows in our _website_ table.

id | hits
-- | --
1 | 9
2 | 10

.pull-left[
.red[Thread A]
```SQL
BEGIN;
UPDATE website SET hits = hits + 1;
COMMIT;
--
--
```
]

.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
DELETE FROM website WHERE hits = 10;
--
COMMIT;
```
]

What happens?

---

# Nonrepeatable read problems

This can happen in the _Read committed_ level:

.pull-left[
Initial table:

id | hits
-- | --
1 | 9
2 | 10
]

.pull-right[
Final table:

id | hits
-- | --
1 | 10
2 | 11
]

.pull-left[
.red[Thread A]
```SQL
BEGIN;
UPDATE website SET hits = hits + 1;
COMMIT;
--
--
```
]

.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
DELETE FROM website WHERE hits = 10;
--
COMMIT;
```
]

1. _Thread B_ doesn't see update from _Thread A_. It checks what rows to delete, and decides to skip `id = 1`.
2. Row with `id = 2` is locked, so _Thread B_ waits _Thread A_ to commit;
3. _Thread B_ gets the lock, it re-checks the `hits = 10` condition, and it's no longer true.

---

# "Solving" Nonrepeatable read problems

If isolation level is equal or higher than _Repeatable read_:

.pull-left[
.red[Thread A]
```SQL
BEGIN;
UPDATE website SET hits = hits + 1;
COMMIT;
--
--
--
```
]

.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
DELETE FROM website WHERE hits = 10;
COMMIT;
-- ERROR: could not serialize access
--        due to concurrent update
```
]

Error stops this from happening. Code needs to retry transaction.

With _Repeatable Read_, the database is frozen to what it was at the time the _Transaction_ began.

---

# Phantom read

<blockquote class="blockquote">
A transaction re-executes a query returning a set of rows that satisfy a search condition and finds that the set of rows satisfying the condition has changed due to another recently-committed transaction.
</blockquote>

With _Repeatable read_ isolation level, this happens in SQL Server, but not in PostgreSQL or MySQL.

.pull-left[
.red[Thread A]
```SQL
BEGIN;
SELECT id FROM accounts
  WHERE balance > 0; -- [1, 2]
--
--
--
SELECT id FROM accounts
  WHERE balance > 0; -- [1, 2, 3]
COMMIT;
```

]
.pull-right[
.red[Thread B]
```SQL
BEGIN;
--
--
INSERT INTO accounts (balance) VALUES (200);
COMMIT;
--
--
--
--
```
]

MySQL and PostgreSQL implementation uses Multiversion Concurrency Control (MVCC), which means inside a transaction, the client always sees a snapshot of the Database from the time it began.
---

# Serialization anomaly

<blockquote class="blockquote">
The result of successfully committing a group of transactions is inconsistent with all possible orderings of running those transactions one at a time.
</blockquote>

Assume we have the table of on-call developers:

name     | on call?
-------- | --------
Danylo   | true
Mykhailo | true
Sergii   | false

1. Danylo is not feeling well, so he decides to leave on call this time. On-call system accepts it, because there is still someone on call.
2. At the same time, Mykhailo is not feeling well either, and concurrently leaves on-call. Danylo hasn't commited his transaction yet, so the on-call system allows him to leave on-call.
3. Nobody is on-call!

---

# Serialization anomaly in code

.pull-left[
.red[Thread A]
```SQL
BEGIN;
SELECT COUNT(on_call) FROM devs -- [2]
UPDATE devs SET on_call = false
  WHERE name = 'Danylo'
COMMIT;
```

]
.pull-right[
.red[Thread B]
```SQL
BEGIN;
SELECT COUNT(on_call) FROM devs -- [2]
UPDATE devs SET on_call = false
  WHERE name = 'Mykhailo'
COMMIT;
```
]

<div style="clear: both;">No phantom read happened. No issues with repeatable read either.</div>

---

# Solving Serialization anomaly

If isolation level is _Serializable_:

.pull-left[
.red[Thread A]
```SQL
BEGIN;
SELECT COUNT(on_call) FROM devs -- [2]
UPDATE devs SET on_call = false
  WHERE name = 'Danylo'
COMMIT;
--
--
```

]
.pull-right[
.red[Thread B]
```SQL
BEGIN;
SELECT COUNT(on_call) FROM devs -- [2]
UPDATE devs SET on_call = false
  WHERE name = 'Mykhailo'
COMMIT;
-- ERROR: could not serialize access due to
-- read/write dependencies among transactions
```
]

<div style="clear: both;">Again, an error stops this from happening. Code potentially needs even more retry logic.</div>

PostgreSQL is smart about serializable; it doesn't use blocking locks. It keeps track of what's going on, and if an anomaly happened, it rollsback one of the transactions.

---

# Isolation levels (the ANSI/ISO standard)

Isolation Level  | Dirty Read   | Nonrepeatable Read | Phantom Read | Serialization Anomaly |
---------------- | ------------ | ------------------ | ------------ | --------------------- |
Read uncommitted | Allowed*     | Possible           | Possible     | Possible              |
Read committed   | Not possible | Possible           | Possible     | Possible              |
Repeatable read  | Not possible | Not possible       | Allowed**    | Possible              |
Serializable     | Not possible | Not possible       | Not possible | Not possible          |

.footnote[
  <p>* Not possible in PostgreSQL</p>
  <p>** Not possible in PostgreSQL or MySQL</p>
]

---

# Explicit locking

There are other ways of dealing with concurrency.

1. Lock the whole table or the rows you want
2. Row-level locks (e.g. `SELECT FOR UPDATE`)
3. Advisory locks that need to be released manually (`SELECT pg_advisory_lock(id) ...`)

---

# References

- [Mark McDonald - Fixing a race condition](https://medium.com/in-the-weeds/fixing-a-race-condition-c8b475fbb994)
- [PostgreSQL - Transaction isolation](https://www.postgresql.org/docs/9.6/transaction-iso.html)
- [PostgreSQL - Advisory locks](https://www.postgresql.org/docs/9.6/explicit-locking.html#ADVISORY-LOCKS)
- [Martin Kleppmann - Transactions: myths, surprises and opportunities](https://www.youtube.com/watch?v=5ZjhNTM8XU8)

---

class: middle, center

# Questions?


    </textarea>
    <script src="/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>
